import os
import sys
import asyncio
import snowflake.client

from models.ticker import Model as Ticker

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import ccxt.async as ccxt  # noqa: E402

import redis

import json

from reformers.ticker import huobiTickerMapping, okexTickerMapping

from cryptocache import redis_center

snowflake.client.setup('snowflake', 8910)

engine = create_engine('mysql+pymysql://root:bitsee@bitsee.cc:3306/october_cms?charset=utf8')
            
DBSession = sessionmaker(bind=engine)
            
session = DBSession()

redis_inst = redis.Redis(connection_pool=redis_center.redis_tool)
        
okex = ccxt.okex()

datas = asyncio.get_event_loop().run_until_complete(okex.fetch_tickers())

print("GGGGGGGG!!!!!!!!!!")

"""

print(datas)


for k in datas:
    ticker = Ticker(okexTickerMapping(datas[k]))
    session.add(ticker)
"""
ticker_json = json.dumps(datas)

ticker_json = 's:'+str(len(ticker_json))+':\"'+ticker_json + '\";'

redis_inst.set('laravel:'+'ticker_okex', ticker_json)

huobi  = ccxt.huobipro()

data = asyncio.get_event_loop().run_until_complete(huobi.fetch_ticker('BTC/USDT'))

ticker_json = json.dumps(data)

ticker_json = 's:'+str(len(ticker_json))+':\"'+ticker_json + '\";'

redis_inst.set('laravel:'+'ticker_huobipro_'+'BTC/USDT', ticker_json)



ticker = Ticker(huobiTickerMapping(data))


data = asyncio.get_event_loop().run_until_complete(huobi.fetch_ohlcv('BTC/USDT','15min'))

ohlcv_json = json.dumps(data)

ohlcv_json = 's:'+str(len(ohlcv_json))+':\"'+ohlcv_json + '\";'

redis_inst.set('laravel:'+'ohlcv_huobipro_'+'BTC/USDT', ohlcv_json)

"""
session.add(ticker)
    
session.commit()
"""

session.close()