import datetime
from dateutil import tz
from dateutil.tz import tzlocal

from sqlalchemy.dialects.mysql import \
        BIGINT, BINARY, BIT, BLOB, BOOLEAN, CHAR, DATE, \
        DATETIME, DECIMAL, DECIMAL, DOUBLE, ENUM, FLOAT, INTEGER, \
        LONGBLOB, LONGTEXT, MEDIUMBLOB, MEDIUMINT, MEDIUMTEXT, NCHAR, \
        NUMERIC, NVARCHAR, REAL, SET, SMALLINT, TEXT, TIME, TIMESTAMP, \
        TINYBLOB, TINYINT, TINYTEXT, VARBINARY, VARCHAR, YEAR

def getdate(x):
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('CST-8')
    ret = datetime.datetime.strptime(x['datetime'],'%Y-%m-%dT%H:%M:%S.%fZ')
    ret = ret.replace(tzinfo=from_zone).astimezone(to_zone)
    return ret       
def huobiTickerMapping(dict):
    mappings = {
        'high':    lambda x:x['high'],
        'low':     lambda x:x['low'],
        'published_at': getdate,
        'vol':     lambda x:x['info']['vol'],
        'open':    lambda x:x['open'],
        'close':   lambda x:x['close'],
        'symbol':  lambda x:x['symbol'],
        'published_stamp': lambda x:x['timestamp']
    }
    
    result = {'market':'huobipro'}
    
    for k in mappings:
        result[k] = mappings[k](dict)
    return result   
def okexTickerMapping(dict):
    
    mappings = {
        'high':    lambda x:x['high'],
        'low':     lambda x:x['low'],
        'published_at': getdate,
        'vol':     lambda x:x['info']['vol'],
        'open':    lambda x:x['open'],
        'close':   lambda x:x['close'],
        'symbol':  lambda x:x['symbol'],
        'published_stamp': lambda x:x['timestamp']
    }
    
    result = {'market':'cokex'}
    
    for k in mappings:
        result[k] = mappings[k](dict)
    
    return result 
        
    
    
    