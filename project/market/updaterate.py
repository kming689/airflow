import os
import sys
import urllib.request
import urllib.parse
import json
import redis

from cryptocache import redis_center

redis_inst = redis.Redis(connection_pool=redis_center.redis_tool)

request=urllib.request.Request('http://www.apilayer.net/api/live?access_key=5e5cbab958fce606c9b7dada5f7da907&format=1')
result=urllib.request.urlopen(request).read().decode("utf-8") 

obj = json.loads(result)

result = json.dumps(obj['quotes']['USDCNY'])

result_json = 's:'+str(len(result))+':\"'+result + '\";'

redis_inst.set('laravel:exchange_rate_usdtocny', result_json)

test = redis_inst.get('laravel:exchange_rate_usdtocny')

print(test)
