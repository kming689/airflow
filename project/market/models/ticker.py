from sqlalchemy import Column, String, DateTime, INT, create_engine, func
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import time

from sqlalchemy.dialects.mysql import \
        BIGINT, BINARY, BIT, BLOB, BOOLEAN, CHAR, DATE, \
        DATETIME, DECIMAL, DECIMAL, DOUBLE, ENUM, FLOAT, INTEGER, \
        LONGBLOB, LONGTEXT, MEDIUMBLOB, MEDIUMINT, MEDIUMTEXT, NCHAR, \
        NUMERIC, NVARCHAR, REAL, SET, SMALLINT, TEXT, TIME, TIMESTAMP, \
        TINYBLOB, TINYINT, TINYTEXT, VARBINARY, VARCHAR, YEAR

import snowflake.client

Base = declarative_base()

class Model(Base):
    '''
    classdocs
    '''
    __tablename__ = 'tickers'
    id    = Column(BIGINT, primary_key=True)
    high  = Column(DOUBLE)
    low   = Column(DOUBLE)
    vol   = Column(DOUBLE)
    open  = Column(DOUBLE)
    close = Column(DOUBLE)
    symbol= Column(CHAR(50))
    market= Column(ENUM('huobipro', 'cokex'))
    published_at = Column(DateTime)
    published_stamp = Column(BIGINT)
    created_at = Column(TIMESTAMP())
    updated_at = Column(TIMESTAMP())
    

    def __init__(self, params):
        '''
        Constructor
        '''
        self.id = snowflake.client.get_guid()
        self.high = params['high']
        self.low = params['low']
        self.vol = params['vol']
        self.open = params['open']
        self.symbol = params['symbol']
        self.market = params['market']
        self.published_at = params['published_at']
        timestamp = func.current_timestamp()
        self.created_at = timestamp
        self.updated_at = timestamp
        self.published_stamp = params['published_stamp']